/**
 * University of San Diego
 * COMP 285
 * Spring 2015
 * Instructor: Gautam Wilkins
 *
 * This class implements Huffman codes for files containing ASCII characters. It can read and compress a file and also
 * decode a compressed file and covert it back to its original ASCII characters.
 */

import com.sun.deploy.util.StringUtils;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        // for each filename in args, perform compression and record sizes
        for (int i = 0; i < args.length; i++) {
            String fname = args[i].substring(0, args[i].length()-4);
            System.out.printf("\n%-28s %d\n", fname + ".txt comparison:",
                    test(fname + ".txt", fname + "-htree.txt", fname + "-encoded.txt"));
            System.out.printf("%-28s %d %s\n", fname + ".txt size:",
                    (new File(fname + ".txt")).length(), "bytes");
            System.out.printf("%-28s %d %s\n", fname + "-encoded.txt size:",
                    (new File(fname + "-encoded.txt")).length(), "bytes");
            System.out.printf("%-28s %d %s\n", fname + "-htree.txt size:",
                    (new File(fname + "-htree.txt")).length(), "bytes");
        }
        System.exit(0);
    }

    private static int test(String inputFile, String hTreeFile, String byteFile) {
        String fileContents = encode(inputFile, hTreeFile, byteFile);
        String original = decode(hTreeFile, byteFile);
        // check that it matches the original encoded string
        return original.compareTo(fileContents);
    }

    /**
     * Method to encode a text file into a huffman tree and frequency hashmap
     * @param inputFile the file to be encoded
     * @param hTreeFile the output file to store the huffman tree
     * @param encodedFile the output file to store the encoded text
     * @return contents of inputFile
     */
    private static String encode(String inputFile, String hTreeFile, String encodedFile) {
        // Read characters from input file
        ArrayList<Character> characterArrayList = HuffmanCode.charactersFromFile(inputFile);
        String fileContents = "";
        for (Character c : characterArrayList) {
            fileContents = fileContents.concat(c.toString());
        }

        // Create hashmap for character frequencies
        HashMap<Character, Integer> freq = HuffmanCode.frequenciesFromArray(characterArrayList);
        // Create huffman tree from frequencies hashmap
        HuffmanTree fullTree = HuffmanCode.generateCodingTree(freq);

        // Generate encodings for the characters
        HashMap<Character, ArrayList<Boolean>> encoding = fullTree.getEncodings();
        String encodedString = HuffmanCode.encodeCharacters(characterArrayList, encoding);

        // Convert binary strings to byte arrays
        byte[] byteArray = BinaryHelper.byteArrayFromBitString(encodedString);
        byte[] huffmanTreeEncoding = HuffmanTree.huffmanTreeToByteArray(fullTree);

        // Write byte arrays to file
        BinaryHelper.writeBytesToFile(hTreeFile, huffmanTreeEncoding);
        BinaryHelper.writeBytesToFile(encodedFile, byteArray);

        return fileContents;
    }

    /**
     * Method to decode a file that has been encoded with a huffman tree
     * @param hTreeFile the input file where huffman tree is stored
     * @param encodedFile the input encoded file
     * @return the decoded String
     */
    private static String decode(String hTreeFile, String encodedFile) {
        // Decode strings from file
        byte[] testByteArray = BinaryHelper.readBytesFromFile(encodedFile);
        String encodedStringFromFile = BinaryHelper.bitStringFromByteArray(testByteArray);

        byte[] huffmanTreeEncoded = BinaryHelper.readBytesFromFile(hTreeFile);
        HuffmanTree treeFromFile = HuffmanTree.huffmanTreeFromByteArray(huffmanTreeEncoded);

        return HuffmanTree.decodeBinaryString(encodedStringFromFile, treeFromFile);
    }
}
